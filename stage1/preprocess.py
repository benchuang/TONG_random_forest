
def prepare_dataset(dir_path, seq):
        df =[]
        with open(os.path.join(dir_path, seq, 'material.JSON')) as f:
            data = json.loads(f.read())['20*20']
        data = np.array(data).reshape([20,20]) 
        label= []
        for r_index, row in enumerate(data):
            for e_index, e in enumerate(row):
                label.append((e, r_index, e_index, seq))        
        ims = []
        for n_index, i in enumerate(num):
            filepath = os.path.join(dir_path, seq, i)        
            with open(filepath) as f:
                data = json.loads(f.read())['21*21']
            data = np.array(data).reshape([21,21])            
            for r_index, row in enumerate(data):
                for e_index, e in enumerate(row):
                    ims.append((e, r_index, e_index, n_index, seq))

        for y in range(20):
            for x in range(20): 
                same_wp = []
                same_wp.append(y)
                same_wp.append(x)
                for row in ims:
                    if row[1]==y and row[2]==x:
                        same_wp.append(row[0])
                    if row[1]==y+1 and row[2]==x:
                        same_wp.append(row[0])
                    if row[1]==y and row[2]==x+1:
                        same_wp.append(row[0])
                    if row[1]==y+1 and row[2]==x+1:
                        same_wp.append(row[0])
                for row in label:
                    if row[1]==y and row[2]==x:
                        same_wp.append(row[0])
                df.append(same_wp)
        df = pd.DataFrame(df)
        return df[df.columns[0:82]], df[df.columns[82]]