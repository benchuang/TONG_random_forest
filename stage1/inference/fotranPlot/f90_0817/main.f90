subroutine tecplot_fig_python(interface_fig_inf,interface_outfile_inf) bind(c, name="tec_fig_2D")
use, intrinsic :: ISO_C_BINDING
implicit none

type, BIND(C) :: interface_fig
   integer (C_INT) :: no_x,no_y
   type (C_PTR) fig
   integer (C_INT) check_data_format
end type interface_fig

type, BIND(C) :: interface_outfile
   integer (C_INT) :: file_ind
end type interface_outfile

type (interface_fig), intent(in) :: interface_fig_inf
type (interface_outfile), intent(in) :: interface_outfile_inf

!!!!!!!!!!!!!!!!!! pointer interface_fig_inf
real (C_DOUBLE), pointer:: fig(:)
!!!!!!!!!!!!!!!!!! pointer interface_fig_inf


real*8, allocatable:: x(:),y(:)
integer*4 i,j,NB,nx,ny

x=0.0d0
y=0.0d0

nx=interface_fig_inf%no_x
ny=interface_fig_inf%no_y

allocate(x(nx*ny),y(nx*ny))

open(99,file='temp.dat')
write(99,*) 'check_data_format',interface_fig_inf%check_data_format
write(99,*) 'no_x',interface_fig_inf%no_x
write(99,*) 'no_y',interface_fig_inf%no_y

do j=1,ny
   do i=1,nx
      NB=NB+1
      x(NB)=(i-1)*1.0d0
      y(NB)=(j-1)*1.0d0
      write(99,*) NB,x(NB),y(NB)
   end do
end do
close(99)

if (interface_fig_inf%check_data_format==0) then  !node format

!!!!!!!!!!!!!!!!! C_F_POINTER  interface_polygon_inf
call C_F_POINTER(interface_fig_inf%fig, fig, [interface_fig_inf%no_x*interface_fig_inf%no_y])
!!!!!!!!!!!!!!!!! C_F_POINTER  interface_polygon_inf

call tecplot_fig(&
!interface_fig_inf
& interface_fig_inf%no_x,&
& interface_fig_inf%no_y,&
& x,y,fig,&
!interface_outfile_inf
& interface_outfile_inf%file_ind)

end if


if (interface_fig_inf%check_data_format==1) then   ! element format
!!!!!!!!!!!!!!!!! C_F_POINTER  interface_polygon_inf
call C_F_POINTER(interface_fig_inf%fig, fig, [(interface_fig_inf%no_x-1)*(interface_fig_inf%no_y-1)])
!!!!!!!!!!!!!!!!! C_F_POINTER  interface_polygon_inf

call tecplot_fig_center(&
!interface_fig_inf
!& interface_fig_inf%no_x,&
!& interface_fig_inf%no_y,&
& nx,&
& ny,&
& x,y,fig,&
!interface_outfile_inf
& interface_outfile_inf%file_ind)

end if

end subroutine

    
