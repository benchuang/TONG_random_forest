subroutine tecplot_fig(&
!interface_fig_inf
& no_x,&
& no_y,&
& x,y,fig,&
!interface_outfile_inf
& file_ind)

integer*4 ind
integer*4 i,j,k
integer*4 no_x,no_y
real*8 x(no_x*no_y),y(no_x*no_y),fig(no_x*no_y)
integer*4 file_ind
character(256) filename

write(filename,"(A3,I7,A4)") 'RES',file_ind,'.dat'

ind=333

open(ind,file=filename)
write(ind,"(A27)") 'Title=" 3-D Model Results "'
write(ind,"(A34)") 'VARIABLES="x(m)","y(m)","K(m/day)"'
write(ind,*) 'ZONE I=',no_x,', J=',no_y,', F=Point'
do j=1,no_y
   do i=1,no_x
      write(ind,*) x((j-1)*no_x+i),y((j-1)*no_x+i),fig((j-1)*no_x+i)
   end do
end do

close(333)
return
end 



subroutine tecplot_fig_center(&
!interface_fig_inf
& no_x,&
& no_y,&
& x,y,fig,&
!interface_outfile_inf
& file_ind)

integer*4 ind
integer*4 i,j,k
integer*4 no_x,no_y
real*8 x(no_x*no_y),y(no_x*no_y),fig((no_x-1)*(no_y-1))
integer*4 file_ind
character(256) filename

write(filename,"(A3,I7,A4)") 'RES',file_ind,'.dat'

ind=333

open(ind,file=filename)
write(ind,"(A27)") 'Title=" 3-D Model Results "'
write(ind,"(A34)") 'VARIABLES="x(m)","y(m)","K(m/day)"'
write(ind,*) 'ZONE I=',no_x,', J=',no_y,', F=BLOCK'
write(ind,"(A30)") 'VARLOCATION=([3]=CELLCENTERED)'
write(ind,"(5(E,1x))") (x(i),i=1,no_x*no_y)
write(ind,"(5(E,1x))") (y(i),i=1,no_x*no_y)
write(ind,"(5(E,1x))") (fig(i),i=1,(no_x-1)*(no_y-1))

close(333)
return
end 
