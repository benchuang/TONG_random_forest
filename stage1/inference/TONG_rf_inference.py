import re, os, json, time, shutil, sys
import logging
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=FutureWarning) 
warnings.filterwarnings("ignore", category=UserWarning) 

import numpy as np
import pandas as pd

import scipy
import scipy.io
import scipy.misc as misc
from scipy.stats import spearmanr, pearsonr

from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_regression
from sklearn.metrics import accuracy_score
from sklearn.metrics import r2_score
from sklearn.externals import joblib

from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

# plot 
import imp 
util = imp.load_source('f_plot_obj', 'fotranPlot/fPlotFuntion.py') 
import f_plot_obj

# logging setting
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s : %(message)s', filename='inference.log')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s : %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

global NE
global TRAINING_SET 
global PAGE
global DATASET_PATH
global RESULT_PATH
global MODEL_NAME

# # n_estimators
# NE = sys.argv[2]

# # traning model 
# TRAINING_SET = sys.argv[1]

# # DEFAULT
# PAGE = 20
# DATASET_PATH = '/home/cuda/DatasetDirectory'
# RESULT_PATH = '/home/cuda/ResultDirectory'

# config 
if os.name == 'nt':
    sys.path.append('..\\..')
else:
    sys.path.append('../..')
from config import TONG_config
config_file = sys.argv[1]
config = TONG_config(config_file)

# Data directory
dir_path = config.dir_path

# Dataset range
DATASET_PATH = config.inference['DATASET_PATH']
RESULT_PATH = config.inference['RESULT_PATH']
if ( not os.path.exists(DATASET_PATH) and not os.path.exists(RESULT_PATH)):
    os.mkdir(DATASET_PATH)
    os.mkdir(RESULT_PATH)
# Inference Parameters
NE = config.inference['ne']
PAGE = config.inference['PAGE']
TRAINING_SET = config.inference['TRAINING_SET']
models_dir = config.models_dir

# rf_(traingsize)_(page)_(n_estimators)
MODEL_NAME = 'rf_{0}_{1}_{2}'.format(TRAINING_SET, PAGE, NE)
MODEL_PATH = os.path.join(models_dir, MODEL_NAME)
print(MODEL_NAME)


def prepare_dataset(validation_path):
        validation_name = [e for e in validation_path.split(os.path.sep) if e is not ''][-1]    
        num = range(1, PAGE+1)
        num = ['Stress'+str(i).zfill(2)+'.JSON' for i in num]
        df =[]
        with open(os.path.join(validation_path, 'material.JSON')) as f:
            data = json.loads(f.read())['20*20']
        data = np.array(data).reshape([20,20]) 
        label= []
        for r_index, row in enumerate(data):
            for e_index, e in enumerate(row):
                label.append((e, r_index, e_index, validation_name))        
        ims = []
        for n_index, i in enumerate(num):
            filepath = os.path.join(validation_path, str(i))        
            with open(filepath) as f:
                data = json.loads(f.read())['21*21']
            data = np.array(data).reshape([21,21])            
            for r_index, row in enumerate(data):
                for e_index, e in enumerate(row):
                    ims.append((e, r_index, e_index, n_index, validation_name))
                    
        # feature extraction
        for y in range(20):
            for x in range(20): 
                same_wp = []
                same_wp.append(y)
                same_wp.append(x)
                for row in ims:
                    if row[1]==y and row[2]==x:
                        same_wp.append(row[0])
                    if row[1]==y+1 and row[2]==x:
                        same_wp.append(row[0])
                    if row[1]==y and row[2]==x+1:
                        same_wp.append(row[0])
                    if row[1]==y+1 and row[2]==x+1:
                        same_wp.append(row[0])
                for row in label:
                    if row[1]==y and row[2]==x:
                        same_wp.append(row[0])
                df.append(same_wp)
        df = pd.DataFrame(df)
        
        logging.info('Prepare validation: {}'.format(validation_path) )
        return df[df.columns[0:82]], df[df.columns[82]]



def plot_stress(validation_path):
        validation_name = [e for e in validation_path.split(os.path.sep) if e is not ''][-1]
        logging.info('Plot stress: {}'.format(validation_path))
        format = 0
        
        if not os.path.exists(os.path.join(RESULT_PATH, validation_name)  ):
            os.mkdir(os.path.join(RESULT_PATH, validation_name) )
            
        stress_names = ['Stress'+str(i).zfill(2)+'.JSON' for i in range(1,PAGE+1)]
        for n_index, stress_name in enumerate(stress_names):
            filepath = os.path.join(validation_path, stress_name)        
            with open(filepath) as f:
                data = json.loads(f.read())['21*21']
                
            f_plot_obj.f_plot_obj.f90_plot(file_num=30, pixel_list=data, format=format, x=21, y=21)
            file = [item for item in os.listdir() if 'RES' in item][0]
            os.rename(file, stress_name.replace('.JSON','.dat'))
            shutil.move(stress_name.replace('.JSON','.dat'), 
                        os.path.join(RESULT_PATH, validation_name, stress_name.replace('.JSON','.dat')))
            
            logging.debug(stress_name)


def inference(validation_path):
    num = range(1, PAGE)
    num = ['Stress'+str(i).zfill(2)+'.JSON' for i in num]
    validation_name = [e for e in validation_path.split(os.path.sep) if e is not ''][-1]    
        
    # Predict start time
    start = time.time()

    val_X, val_y = prepare_dataset(validation_path)
    predicted_dataset = rf.predict(val_X)

    # Predict end time
    end = time.time()
    elapsed = end - start
    logging.info('Predict {} spends {} seconds'.format(validation_name, round(elapsed, 2)))

    
    # Calculate the absolute errors
    errors = abs(predicted_dataset - val_y)

    # Print out the mean absolute error (mae)
#     print('Mean_Absolute_Error:', round(np.mean(errors), 2), 'degrees.')

    # Calculate mean absolute percentage error (MAPE)
    mape = 100 * (errors / val_y)

    # Calculate and display accuracy
    accuracy = 100 - np.mean(mape)
#     print('Accuracy:', round(accuracy, 2), '%.')

    # Reshape array to 2-d
    image = np.array(predicted_dataset).reshape([20,20])
    val_image = np.array(val_y).reshape([20,20])
    
    creteria = dict(
        test_score = r2_score(val_y, predicted_dataset),
        spearman = list(spearmanr(val_y, predicted_dataset)),
        pearson = list(pearsonr(val_y, predicted_dataset)),
        Mean_Absolute_Error = round(np.mean(errors), 2),
        Accuracy = round(accuracy, 2)
        )
    
    logging.info(creteria)
    
    
    # Move to directory
    DIR_NAME = os.path.join(RESULT_PATH, MODEL_NAME, validation_name)
    if not os.path.exists(DIR_NAME):
        os.mkdir(DIR_NAME)

    # output predict .dat 
    f_plot_obj.f_plot_obj.f90_plot(file_num=30, pixel_list=predicted_dataset, format=1, x=21, y=21)
    file = [item for item in os.listdir() if 'RES' in item][0]
    os.rename(file, 'predict.dat')
    logging.debug('predict.dat write finish!')

    # output val_image .dat
    f_plot_obj.f_plot_obj.f90_plot(file_num=30, pixel_list=val_y, format=1, x=21, y=21)
    file = [item for item in os.listdir() if 'RES' in item][0]
    os.rename(file, 'validation.dat')
    logging.debug('validation.dat write finish!')

    # output predict and val raw data .txt
    with open(os.path.join(DIR_NAME ,'valid_predict.txt'),'w') as f:
        f.write(' '.join(val_y.astype(str))+'\n')
        f.write(' '.join(predicted_dataset.astype(str))+'\n')
    logging.debug('valid_predict.txt write finish!')

    # output difference data
    diff_list = predicted_dataset-val_y
    f_plot_obj.f_plot_obj.f90_plot(file_num=30, pixel_list=diff_list, format=1, x=21, y=21)
    file = [item for item in os.listdir() if 'RES' in item][0]
    os.rename(file, 'diff.dat')
#     with open(os.path.join(DIR_NAME ,'diff.txt'),'w') as f:
#         f.write(' '.join(diff_list.astype(str))+'\n')
    logging.debug('Diff write finish!')
    
    for move_file in ['predict.dat', 'validation.dat', 'diff.dat']:
        shutil.move(move_file, os.path.join(DIR_NAME, move_file))


# # Main program

# load start time
start = time.time()
if os.path.exists(MODEL_PATH):
    logging.info('Load model {}...'.format(MODEL_PATH))
    rf = joblib.load(MODEL_PATH)
    if not os.path.exists(os.path.join(RESULT_PATH, MODEL_NAME)):
        os.mkdir(os.path.join(RESULT_PATH, MODEL_NAME))  
else:
    raise AssertisonError('model not found')
# load end time
end = time.time()
elapsed = end - start
logging.info('Load model {} seconds'.format(round(elapsed,2)))

class MyHandler(FileSystemEventHandler):
    def on_created(self, event):
        if event.is_directory:
            logging.info("directory created:{0}".format(event.src_path))
        else:
            path_sep_list = event.src_path.split(os.path.sep)

            if 'fin' in path_sep_list:
                logging.info("file created:{0}".format(event.src_path))
                # ['dataset-dir', 'F0000003']
                print(path_sep_list[:len(path_sep_list)-1])
                #  dataset-dir\F0000003
                print(os.path.sep.join(path_sep_list[:len(path_sep_list)-1]))

                if len(os.listdir(os.path.sep.join(path_sep_list[:len(path_sep_list)-1]))) >= 22:
                    dir_path = event.src_path.replace('fin', '')
                    print(dir_path)
                
                    logging.info('Inference start {}'.format(dir_path))
                    plot_stress(dir_path)
                    inference(dir_path)
                    logging.info('Inference finish {}'.format(dir_path))
                else:
                    logging.info('File in {} are less than 22'.format(dir_path))
            
            
if __name__ == "__main__":
    logging.info("Inference Start!")
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, path='./', recursive=False)
    observer.schedule(event_handler, path=DATASET_PATH, recursive=True)
    observer.start()
    
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

