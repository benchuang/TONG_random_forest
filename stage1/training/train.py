
import os, re, time
import json, sys

import pandas as pd
import numpy as np

import scipy
import scipy.io
import scipy.misc as misc
from scipy.stats import spearmanr, pearsonr

import sklearn
from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler
from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import r2_score
from sklearn.externals import joblib
import shutil

# plot by fortran method
import imp 
util = imp.load_source('f_plot_obj', 'fotranPlot/fPlotFuntion.py') 
import f_plot_obj

# config 
if os.name == 'nt':
    sys.path.append('..\\..')
else:
    sys.path.append('../..')
from config import TONG_config
config_file = sys.argv[1]
config = TONG_config(config_file)

# dir_path = r'/home/cuda/benchuang/TONG_well_inverse'
# _start = 1
# _seqs = 2001
# _num = 21
# RNG=3000
# validation_seq=9002
# ne = 50


# Data directory
dir_path = config.dir_path

# Dataset range
_start = config.dataset['_start']
_seqs = config.dataset['_seqs']
_num = config.dataset['_num']
RNG = config.dataset['RNG']
_seqs = RNG+1

# Training Parameters
ne = config.training['ne']

# Models path
models_dir = config.models_dir
if not os.path.exists(models_dir):
    os.mkdir(models_dir)

# filename, example F0000001, F0000002
seqs = range(_start,_seqs)
seqs = ['F'+str(i).zfill(7) for i in seqs]

num = range(1,_num)
num = ['Stress'+str(i).zfill(2)+'.JSON' for i in num]

# preprocess start time
start = time.time()

# image extraction
if os.path.exists('TONG_{0}_training.pickle'.format(_seqs-1)):
    dataframe=pd.read_pickle('TONG_{0}_training.pickle'.format(_seqs-1))
    print(np.shape(dataframe))
else:
    dataframe = []
    for s_index, seq in enumerate(seqs):
            print(seq)
            with open(os.path.join(dir_path, seq, 'material.JSON')) as f:
                data = json.loads(f.read())['20*20']
            data = np.array(data).reshape([20,20]) 
            
            # label image
            label= []
            for r_index, row in enumerate(data):
                for e_index, e in enumerate(row):
                # Label image introduction
                # e: value. (float)
                # r_index: y axis index. (1-21)
                # e_index: x axis index. (1-21)
                # s_index: page number (1-20)
                    label.append((e, r_index, e_index, s_index))        
            
            # feature image 
            ims = []
            for n_index, i in enumerate(num):
                filepath = os.path.join(dir_path, seq, i)        
                with open(filepath) as f:
                    data = json.loads(f.read())['21*21']
                data = np.array(data).reshape([21,21])
                # Feature image introduction
                # e: value. (float)
                # r_index: y axis index. (1-21)
                # e_index: x axis index. (1-21)
                # s_index: page number (1-20)
                for r_index, row in enumerate(data):
                    for e_index, e in enumerate(row):
                        ims.append((e, r_index, e_index, n_index, s_index))
                        
            # feature extraction from feautre image
            for y in range(20):
                for x in range(20): 
                    same_wp = []
                    same_wp.append(y) # col 0
                    same_wp.append(x) # col 1
                    for row in ims:
                        if row[1]==y and row[2]==x: # col 2, 6, .. 78
                            same_wp.append(row[0])
                        if row[1]==y+1 and row[2]==x: # col 3, 7, .. 79
                            same_wp.append(row[0])
                        if row[1]==y and row[2]==x+1: # col 4, 8, .. 80
                            same_wp.append(row[0])
                        if row[1]==y+1 and row[2]==x+1: # col 5, 9, .. 81
                            same_wp.append(row[0])
                    for row in label:
                        if row[1]==y and row[2]==x:
                            same_wp.append(row[0]) # 82
                    dataframe.append(same_wp)
                    
    # dataframe, the shape of feature and label 
    print(np.shape(dataframe))
    dataframe = pd.DataFrame(dataframe)

    # save the pickle if not exist
    if not os.path.exists('TONG_{0}_training.pickle'.format(_seqs-1)):
        dataframe.to_pickle('TONG_{0}_training.pickle'.format(_seqs-1))

# preprocess end time
end = time.time()
loading_elapsed = end - start

print('loading elapsed time: {0} seconds'.format(loading_elapsed))

# get the specific dataset account
dataframe = dataframe[0:(400*RNG)]
print(np.shape(dataframe))


# Training start time
start = time.time()


# split datasets to training and validation parts.
X_train, X_test, y_train, y_test = train_test_split(dataframe[dataframe.columns[0:82]], dataframe[dataframe.columns[82]], train_size=0.8, random_state=42)

# rf_(traingsize)_(page)_(n_estimators)
model_name = 'rf_{0}_{1}_{2}'.format(_seqs-_start,_num-1,ne)
print(models_dir ,model_name)
model_name = os.path.join(models_dir ,model_name)

if os.path.exists(model_name):
    print('load model {}...'.format(model_name))
    rf = joblib.load(model_name)
else:
    rf = RandomForestRegressor(n_estimators=ne, oob_score=True, random_state=0)
    rf.fit(X_train, y_train)

# evaluation models
predicted_train = rf.predict(X_train)
predicted_test = rf.predict(X_test)
test_score = r2_score(y_test, predicted_test)
spearman = spearmanr(y_test, predicted_test)
pearson = pearsonr(y_test, predicted_test)

# Training end time
end = time.time()
elapsed = end - start

print('predict spends {} seconds'.format(elapsed))
print('predict parameters:')
print(rf.score)

if not os.path.exists(model_name):
    joblib.dump(rf, model_name)

#读取Model
# clf3 = joblib.load('save/clf.pkl')
#测试读取后的Model
# print(clf3.predict(X[0:1]))
# [0]

print('datasets: {0}, n_estimators: {1},training spend {2} seconds'.format(_seqs-1, rf.n_estimators, elapsed))
print('\toob_score: {0}'.format(rf.oob_score_))
print('\tr2_score: {0}'.format(test_score))
print('\tspearman: {0}'.format(spearman))
print('\tpearson: {0}'.format(pearson))

