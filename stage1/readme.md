# 完整範例
從訓練模型至推理一個Sample資料的完整流程。


## 0. 環境
系統: Ubuntu 16.04以上或Linux系統

RAM: 建議8G以上，有關訓練數量

CPU: 建議4 core以上，有關訓練模型速度

DISK: 建議留100G可用空間

## 1. 切換至家目錄
```
cd ~
```

## 2. 下載anaconda於以下連結(Linux版本)，可用wget指令。
https://www.anaconda.com/distribution/

執行安裝
```
wget https://repo.anaconda.com/archive/Anaconda3-2019.07-Linux-x86_64.sh
sh Anaconda3-2019.07-Linux-x86_64.sh
```
已預設值enter或y按下到結束。


## 3. 製作一個python 3.6環境
```
./anaconda3/bin/conda create -n py36 python=3.6 anaconda
```

## 4. 進入python環境
```
source ./anaconda3/bin/activate py36
```

## 5. 下載範例資料
```
mkdir -p F100

cd F100

wget "https://drive.google.com/uc?export=download&id=1POJFdi-p42hWdDVJABZhISPtb9wgL5lk" -O F100.zip

unzip F100.zip
```

![F100](img/F100.PNG)


## 6. 下載TONG_random_forest專案
```
cd ..

git clone https://gitlab.com/benchuang/TONG_random_forest.git
```

## 7. 安裝python套件
```
cd TONG_random_forest
pip install -r requirements.txt
```

## 8. 訓練TONG模型
```
cd stage1/training
python train.py ../config-example.json
```

![train](img/train.PNG)


## 9. 產出的模型位置
```
ls ../models
```
![model](img/model.PNG)


## 10. 推理模型
```
cd ../inference
python TONG_rf_inference.py ../config-example.json
```
![inference](img/inference.PNG)

Step 10開啟inference app時，因為他是持續運行的app，所以不能關掉。
可以在Step 10要開啟另一個terminal繼續執行step11，可以使用tmux。


## 11. 把要推理的資料夾放入
這邊注意一下，第二個terminal的路徑與上一個terminal的路徑是相同的。
```
cp -r ../../../F100/F0000100 dataset-dir/
```

## 12. 放置完成後，寫一個fin的檔案通知推理開始
```
touch dataset-dir/F0000100/fin
```

## 13. 列出運算完的結果
```
ls -l result-dir/rf_10_20_50/F0000100
```
![result](img/result.PNG)


# config配置

## 配置範例
```
{
	"dir_path": "../../../F100",
	"dataset": {
		"_start": 1,
		"_seqs": 11,
		"_num": 21,
		"RNG": 10
	},
	"training": {
		"ne": 50
	},
	"models_dir": "../models",
	"inference": {
		"DATASET_PATH": "dataset-dir",
		"RESULT_PATH": "result-dir",
		"ne": 50,
		"TRAINING_SET": 10,
		"PAGE": 20
	}
}
```

# dir_path
訓練資料集目錄位置，可設置成絕對路徑。

在cuda機器上，可配置`/home/benchuang/cuda/benchuang/TONG_well_inverse`完整資料集到F0010000。

須注意使用大量的資料集訓練會使訓練時間過久，實驗下`4000`可能是cuda台的極限。

# dataset
訓練資料集的參數配置。

## _start
起始資料集，無須調整。

## _seqs
結尾資料集，無須調整。

## _num
Stress的數目，也稱為page，無須調整。

## RNG
訓練的數量，如設置10代表使用F0000001至F0000010訓練。


# training
訓練參數配置。

## ne
random forest的參數，n_estimators，可參考:https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestRegressor.html。

實驗上ne值50為較好的推理結果，模型的檔案也會變大，訓練時間變長。

# models_dir
訓練後模型的放置位置，無須調整。

目錄下的模型名稱為`rf_{RNG}_{page}_{ne}`，例如`rf_10_20_50`。


# inference
推理的參數配置。

## DATASET_PATH
推理的資料集位置，無須調整。

## RESULT_PATH
推理的結果位置，無須調整。

## ne
推理模型使用的ne參數。

## TRAINING_SET
推理模型使用的資料數量，主要目的是對應模型的RNG數量。

## PAGE
推理模型使用Stress的數目，也稱為page，無須調整。

