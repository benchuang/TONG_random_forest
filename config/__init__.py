import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=FutureWarning) 
warnings.filterwarnings("ignore", category=UserWarning) 
import json


class TONG_config:
    def __init__(self, config_file):
        with open(config_file, 'r') as f:
            self.config = json.loads(f.read())
        
        # training parameters
        self.dir_path = self.config.get('dir_path')
        self.models_dir = self.config.get('models_dir')
        self.dataset = self.config.get('dataset')
        self.training = self.config.get('training')

        # inference parameters
        self.inference = self.config.get('inference')