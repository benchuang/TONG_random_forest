from ctypes import *
import ctypes
import sys

import os
import numpy as np
import utils
from pipe import *
from os.path import join


class f_plot_obj(object):


    @staticmethod
    def f90_plot(file_num, pixel_list, format, x=20, y=20):
        sim = cdll.LoadLibrary(join(os.path.dirname(os.path.abspath(__file__)), 'lib', 'PLOT_fig.so'))
        class Param(Structure):
            pass

        class Param2(Structure):
            pass
        no_x = x
        no_y = y
        length = no_x * no_y
        filename = file_num

        Param._fields_ = [("no_x", c_int),
                          ("no_y", c_int),
                          ("fig", POINTER(c_double)),
                          ("check_data_format", c_int)]
        #			  ( "filename", POINTER(c_byte))]
        Param2._fields_ = [("filename", c_int)]

        param = Param()
        param.memShapes = []
        param2 = Param2()
        param2.memShapes = []

        paramC = Param()
        param2C = Param2()

        param.no_x = no_x
        param.memShapes.append({"field_name": "no_x", "dtype": np.int32, "shape": None})
        param.no_y = no_y
        param.memShapes.append({"field_name": "no_y", "dtype": np.int32, "shape": None})
        param.fig = (c_double * length)()
        param.memShapes.append({"field_name": "fig", "dtype": np.float64, "shape": [length]})
        param.check_data_format = format
        param.memShapes.append({"field_name": "check_data_format", "dtype": np.int32, "shape": None})


        # param2.filename = (c_char * 256 )()
        # param2.memShapes.append({"field_name": "filename", "dtype": np.byte, "shape": [256] })
        param2.filename = filename
        param2.memShapes.append({"field_name": "filename", "dtype": np.int32, "shape": None})

	
        if format == 0:
            for i in range(no_x*no_y):
                param.fig[i] = pixel_list[i]
        else:
            for i in range((no_x-1)*(no_y-1)):
                param.fig[i] = pixel_list[i]

        # for idx, c in enumerate(filename ):
        #    param2.filename[idx] = bytes(c, encoding='utf8')

        paramDict = utils.getdict(param)
        utils.getctypes(paramDict, paramC)

        param2Dict = utils.getdict(param2)
        utils.getctypes(param2Dict, param2C)

#        print(paramDict)
#        print("------------------------")
#        print(param2Dict)

        #        null_fds, save = suppress()
        sim.tec_fig_2D(byref(paramC), byref(param2C))
    #        resume(null_fds, save)

if __name__ == "__main__":
    format = 1
    if format == 0:
        pixel_list = range(441)
        x=21
        y=21
    else:
        pixel_list = range(400)
        x=21
        y=21
    f_plot_obj.f90_plot(file_num=30, pixel_list=pixel_list, format=format, x=x, y=y)



