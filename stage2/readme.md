# Stage2

Stage2與Stage1的不同為增加Feature，Stress每個點以lag為距離內的點做covarinance，lag距離從0-29，可以產生30倍的feature，以更大量Feature來做訓練。

4 * 20 * 30 + 2 = 2402

Stress四周點位(4) * Stress Page(20) * lag(30) + x,y軸(2)


## 0. 環境
系統: Ubuntu 16.04以上或Linux系統

RAM: 建議8G以上，有關訓練數量
    8G約能訓練300資料集量。
    16G約能訓練500資料集量。
    29G約能訓練1000資料集量。

CPU: 建議4 core以上，有關訓練模型速度

DISK: 建議留100G可用空間


## 1. 建置python環境

依據stage1的step1-4建置python環境。


## 2. 下載TONG_random_forest專案
```
cd ..

git clone https://gitlab.com/benchuang/TONG_random_forest.git
```

## 3. 安裝python套件
```
cd TONG_random_forest
pip install -r requirements.txt
```


## 4. 前處理
```
python prerocess.py
```


## 5. 前處理資料聚集
```
python extract-preprocess-data.py
```


## 6. 訓練模型

因記憶體的限制會問題可能需要修改系統配置。
https://stackoverflow.com/questions/57507832/unable-to-allocate-array-with-shape-and-data-type

```
echo 1 > /proc/sys/vm/overcommit_memory

python train-covariance-all.py
```



## 7. 推理


