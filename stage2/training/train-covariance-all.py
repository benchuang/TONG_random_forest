
import re
import scipy
import os
import json
import shutil
import time
import numpy as np

import sklearn
from sklearn import preprocessing
from sklearn.ensemble import RandomForestRegressor
from sklearn.datasets import make_regression
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import r2_score
from sklearn.externals import joblib

import scipy.io
from scipy.stats import spearmanr, pearsonr

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=FutureWarning) 
warnings.filterwarnings("ignore", category=UserWarning) 


# Pickle directory
# pickle_dir=r'/home/benchuang/stage2/TONG_training_covariance'
pickle_dir=r'/home/benchuang/stage2'

pickle_files = [os.path.join(pickle_dir, i) for i in os.listdir('./') if i.endswith('covariance.pickle')]
print(pickle_files)

dataframe = pd.concat([pd.read_pickle(i) for i in pickle_files])
print(np.shape(dataframe))

feature_df=dataframe[dataframe.columns[0:2402]]
label_df=dataframe[dataframe.columns[2402]]

# Training start time
start = time.time()

ne = 50

# split datasets to training and validation parts.
X_train, X_test, y_train, y_test = train_test_split(
    feature_df, 
    label_df, 
    train_size=0.8, 
    random_state=42)

# rf_(traingsize)_(page)_(n_estimators)
model_name = 'rf_model_covariance_stage2.pickle'

# Check model existed
if os.path.exists(model_name):
    print('load model {}...'.format(model_name))
    rf = joblib.load(model_name)
else:
    rf = RandomForestRegressor(
        n_estimators=ne, 
        oob_score=True, 
        random_state=0, 
        verbose=1, 
        n_jobs=-1)
    rf.fit(X_train, y_train)
    # Dump model file
    if not os.path.exists(model_name):
        joblib.dump(rf, model_name)

# evaluation models
predicted_train = rf.predict(X_train)
predicted_test = rf.predict(X_test)
test_score = r2_score(y_test, predicted_test)
spearman = spearmanr(y_test, predicted_test)
pearson = pearsonr(y_test, predicted_test)

# Training end time
end = time.time()
elapsed = end - start

print('predict spends {} seconds'.format(elapsed))
print('predict parameters: {}'.format(rf.score))


print('training spend {0} seconds'.format(elapsed))

print('\toob_score: {0}'.format(rf.oob_score_))
print('\tr2_score: {0}'.format(test_score))
print('\tspearman: {0}'.format(spearman))
print('\tpearson: {0}'.format(pearson))


