import numpy as np
import logging
import os
import json

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s : %(message)s', filename='inference.log')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s : %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

def prepare_dataset(dir_path, seq, pages):     
    stress = []
    coordinate = []
    # load file 
    for n_index, i in enumerate(pages):
        # filepath=/home/cuda/benchuang/TONG_well_inverse_new/
        filepath = os.path.join(dir_path, seq, i)        
        with open(filepath) as f:
            data = json.loads(f.read())['21*21']
        data = np.array(data).reshape([21,21])            
        # for r_index, row in enumerate(data):
        #     for e_index, e in enumerate(row):
        #         coordinate.append([r_index, e_index])
        #         stress.append(e)
    return coordinate, stress, data

# def dfun(u, v): 
#     return np.sqrt(((u-v)**2).sum()) 

# def cov(p, p_value, bp, m):
#     return (p_value-m)*(bp[2]-m)

# def covariance(data, point, h):
#     p_value = next(iter([p for p in data if p[0]==point[0] and p[1]==point[1]]), None)    
#     record_p=[p for p in data if dfun(np.array(p[:2]), np.array(point)) <= h ]
#     m = np.mean([p[2] for p in data])
#     c = np.mean([cov(point, p_value[2], p, m) for p in record_p])    
#     return c

# def stress_covariance(data, lag, length=21):
#     # Create Stress convaricance 2-array list
#     cov_list = [[covariance(data, [j,i], lag) for j in range(length)] for i in range(length)]
#     return cov_list

def stress_covariance(data, lag, length=21):
    def covariance(data, point, h):
        def dfun(u, v): 
            return np.sqrt(((u-v)**2).sum()) 
        def cov(p, p_value, bp, m ):
            return (p_value-m)*(bp[2]-m)
        p_value = next(iter([p for p in data if p[0]==point[0] and p[1]==point[1]]), None)    
        record_p=[p for p in data if dfun(np.array(p[:2]), np.array(point)) <= h ]
        m = np.mean([p[2] for p in data])
        c = np.mean([cov(point, p_value[2], p, m) for p in record_p])    
        return c
    # Create Stress convaricance 2-array list
    cov_list = [[covariance(data, [j,i], lag) for j in range(length)] for i in range(length)]
    return cov_list




def stress_cov_process(agrs_list):
    SEQUENCE_FROM=agrs_list[0]
    SEQUENCE_END=agrs_list[1]
    PAGES=agrs_list[2]
    # image square weigth, length [21, 21]
    length=21

    # Directory path
    # dir_path = r"/home/cuda/benchuang/TONG_well_inverse_new/"
    # output_dir_path = r"/home/cuda/benchuang/TONG_well_inverse_covariance"
    dir_path = r'/mnt/home/cuda/benchuang/TONG_well_inverse/'
    output_dir_path = r'TONG_well_inverse_covariance'

    if not os.path.isdir(dir_path):
        raise FileNotFoundError(dir_path)
    if not os.path.isdir(output_dir_path):
        os.mkdir(output_dir_path)

    # sequence_names, example F0000001, F0000002
    seqs = range(SEQUENCE_FROM, SEQUENCE_END+1)
    sequence_names = [seq_name for seq_name in ['F'+str(i).zfill(7) for i in seqs]]

    # pages Stress01.JSON, ... Stress20.JSON
    pages = range(1, PAGES+1)
    page_names = ['Stress'+str(i).zfill(2)+'.JSON' for i in pages]

    # [F0000001, F0000002, ...]
    for seq_name in sequence_names:
        if not os.path.isdir(os.path.join(dir_path, seq_name)):
            raise FileNotFoundError(os.path.join(dir_path, seq_name))
        logging.info('start with {}'.format(seq_name))
        
        if not os.path.exists(os.path.join(output_dir_path, seq_name)):
            # Create /home/cuda/benchuang/TONG_well_inverse_covariance/F0000001
            os.mkdir(os.path.join(output_dir_path, seq_name))

        # [Stress01.JSON, Stress02.JSON ...]
        for page_name in page_names:
            # Check /home/cuda/benchuang/TONG_well_inverse_new/F0000001/Stress01.JSON
            if not os.path.exists(os.path.join(dir_path, seq_name, page_name)):
                raise FileNotFoundError(os.path.join(dir_path, seq_name, page_name))
                
            logging.info('{}, {}, loading...'.format(seq_name, page_name))
            # load data from Stress data
            _,_,data= prepare_dataset(dir_path, seq_name, [page_name])
            # 20 * 20 * [x, y, value]
            lines = [[x,y,data[y][x]] for x in range(length) for y in range(length)]

            # lag 
            for lag in range(0,30):
                results = stress_covariance(lines, lag)    
                # Output /home/cuda/benchuang/TONG_well_inverse_covariance/F0000001/Stress01_lag0.JSON
                with open(os.path.join(output_dir_path, seq_name, '{}_lag{}'.format(page_name, lag)), 'w') as f:
                    f.write(json.dumps(results))
                logging.info('{}, {}, lag={}'.format(seq_name, page_name, lag))


if __name__ == '__main__':
    processers=8
    base=1
    jobs=20
    args_list=[[i*jobs+1, (i+1)*jobs, 20] for i in range(processers)]
    print(args_list)
    # agrs_list=[[1, 20, 20], [21, 40, 20], ...]

    from multiprocessing import Pool
    pool = Pool()
    pool.map(stress_cov_process, args_list)
