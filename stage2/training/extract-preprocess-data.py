import re
import time
import logging
import os
import json

import scipy
import sklearn
import numpy as np
import pandas as pd

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=FutureWarning) 
warnings.filterwarnings("ignore", category=UserWarning) 

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s : %(message)s', filename='test-covariance.log')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s : %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)

def preprocess_covariance(arg_list):    
    dir_path = arg_list['dir_path']
    dir_path_cov = arg_list['dir_path_cov']
    _start = arg_list['_start']
    _seqs = arg_list['_seqs']
    _num = arg_list['_num']
    _lags = arg_list['_lags']

    # filename, example F0000001, F0000002
    seqs = range(_start, _seqs+1)
    seqs = ['F'+str(i).zfill(7) for i in seqs]

    # Stress01.JSON, Stress02.JSON
    num = range(1,_num+1)
    num = ['Stress'+str(i).zfill(2)+'.JSON' for i in num]

    # preprocess start time
    start = time.time()

    # TONG_1_1000_training_covariance.pickle
    pickle_name = 'TONG_{0}_{1}_training_covariance.pickle'.format(_start, _seqs)

    # image extraction
    if os.path.exists(pickle_name):
        dataframe=pd.read_pickle(pickle_name)
        logging.info(np.shape(dataframe))
    else:
        dataframe = []
        for s_index, seq in enumerate(seqs):
                print(seq)
                # Load label file named 'material.JSON'
                with open(os.path.join(dir_path, seq, 'material.JSON')) as f:
                    data = json.loads(f.read())['20*20']
                data = np.array(data).reshape([20,20]) 

                # label image
                label= []
                for r_index, row in enumerate(data):
                    for e_index, e in enumerate(row):
                    # Label image introduction
                    # e: value. (float)
                    # r_index: y axis index. (1-21)
                    # e_index: x axis index. (1-21)
                    # s_index: page number (1-20)
                        label.append([e, r_index, e_index, s_index])        

                #　Load feature Stress_lag file
                stress=[]

                # Stress01.JSON, Stress02.JSON
                for n_index, n in enumerate(num):
                    for lag in range(_lags):
                        # 'Stress01.JSON_lag0', 'Stress01.JSON_lag1'
                        filepath = os.path.join(dir_path_cov, seq, '{0}_lag{1}'.format(n, lag))
                        with open(filepath, 'r') as f:
                            data = json.loads(f.read())
                        # Label image introduction
                        # e: value. (float)
                        # r_index: y axis index. (1-21)
                        # e_index: x axis index. (1-21)
                        # s_index: page number (1-20)
                        for r_index, row in enumerate(data):
                            for e_index, e in enumerate(row):
                                # stress.append([e, r_index, e_index, lag, n_index, s_index])
                                stress.append([e, e_index, r_index, lag, n_index, s_index])


                # Stress-lag and label to dataframe
                for y in range(20):
                    for x in range(20):
                        feature=[]
                        feature.append(y)
                        feature.append(x)
                        for sp in stress:
                            # left-bottom [0, 0] -> [0, 0]
                            if sp[1]==x and sp[2]==y:
                                feature.append(sp[0])
                            # left-up [0, 0] -> [0, 1]
                            if sp[1]==x and sp[2]==y+1:
                                feature.append(sp[0])   
                            # right-bottom [0, 0] -> [1, 0]                      
                            if sp[1]==x+1 and sp[2]==y:
                                feature.append(sp[0])
                            # right-up [0, 0] -> [1, 1]
                            if sp[1]==x+1 and sp[2]==y+1:
                                feature.append(sp[0])
                        for row in label:
                            # if row[1]==y and row[2]==x:
                            if row[1]==x and row[2]==y:
                                feature.append(row[0])                 
                        dataframe.append(feature)
        dataframe = pd.DataFrame(dataframe)
        logging.info(np.shape(dataframe))

        # save the pickle if not exist
        if not os.path.exists(pickle_name):
            dataframe.to_pickle(pickle_name)

    # preprocess end time
    end = time.time()
    loading_elapsed = end - start

    logging.info('loading elapsed time: {0} seconds'.format(loading_elapsed))



if __name__ == '__main__':

    # dir_path = r'/home/cuda/benchuang/TONG_well_inverse_new'
    # dir_path_cov = r'/home/cuda/benchuang/TONG_well_inverse_covariance'
    # _start = 1001
    # _seqs = 4000
    # _num = 20
    # _lags = 30
    # interval=200

    dir_path = r'/mnt/home/cuda/benchuang/TONG_well_inverse/'
    dir_path_cov = r'TONG_well_inverse_covariance'
    _start = 1
    _seqs = 500
    _num = 20
    _lags = 30
    interval = 100    

    # se_list [[1, 100], [101, 200] ...]
    se_list=[[i, i+interval-1] for i in range(_start, _seqs, interval)]
    args_list = [dict(dir_path=dir_path, 
                    dir_path_cov=dir_path_cov,
                    _start=se[0],
                    _seqs=se[1],
                    _num=_num,
                    _lags=_lags
                ) for se in se_list]

    # multithead preprocess
    from multiprocessing import Pool
    pool = Pool()
    logging.info(arg_list)
    pool.map(preprocess_covariance, args_list)
