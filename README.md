# TONG_random_forest

## 簡介

使用random forest來訓練資料集模型，並推理出結果。

## 子專案介紹

![TONG-train-process.png](doc/img/TONG-train-process.png)


## stage1

請參照`doc/基於機器學習水位圖預測地質係數_20181011.docx`。

程式執行範例可參照`stage1/readme.md`。

## stage2


[Stage2介紹](stage2/readme.md)

## stage3


[Stage3介紹](stage3/readme.md)


# 實驗結果(最後更新:2020-01-16)

https://docs.google.com/spreadsheets/d/10w0l5EajX8wtXYYJiqO8VDta1xlSjn2zPVQs3rAqMV0/edit#gid=0


## 小結

stage1訓練的模型最佳，且前處理的時間與訓練時間也相較其餘子專案較短。


# 備註

## Mac install gfortran

https://2formosa.blogspot.com/2017/11/install-gfortran-on-mac.html

