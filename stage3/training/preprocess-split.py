import numpy as np
import logging
import os
import json
import pandas as pd
from functools import reduce

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s : %(message)s', filename='inference.log')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s : %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)


def sequence_dataframe(dir_path, seq, PAGES=20):
    print('Dataset:{}'.format(seq))
    def prepare_dataset(dir_path, seq, pages):     
        stress = []
        coordinate = []
        for n_index, i in enumerate(pages):
            filepath = os.path.join(dir_path, seq, i)        
            with open(filepath) as f:
                data = json.loads(f.read())['21*21']
            data = np.array(data).reshape([21,21])            
            for r_index, row in enumerate(data):
                for e_index, e in enumerate(row):
                    coordinate.append([r_index, e_index])
                    stress.append(e)
        return coordinate, stress, data

    def cov_p2p(x, y, k_matrix):
        point_list = [k_matrix[y][x], 
                    k_matrix[y+1][x], 
                    k_matrix[y][x+1], 
                    k_matrix[y+1][x+1]]
        v_list = []
        # p1 is target point 
        for p1 in point_list:
            pv_list=[]
            for p2 in point_list:
                v_list.append(p1*p2)
        return v_list

    dataframe = []
    pages = range(1, PAGES+1)
    # [Stress01.JSON, Stress02.JSON]
    page_names = ['Stress'+str(i).zfill(2)+'.JSON' for i in pages]
    pages_data = []
    for page_name in page_names:
        _, _, data = prepare_dataset(dir_path, seq, [page_name])
        pages_data.append(data)
    
    # Load label
    with open(os.path.join(dir_path, seq, 'material.JSON')) as f:
        material_data = json.loads(f.read())['20*20']
    material_data = np.array(material_data).reshape([20,20]) 

    for y in range(20):
        for x in range(20):
            feature_list = []
            for p in range(len(pages_data)):
                feature_list = feature_list + cov_p2p(x, y, pages_data[p])
            feature_list = [x, y] + feature_list
            # [0, 0, c1 ,c2... m], [1, 0, c1 ,c2... m] ...
            feature_list.append(material_data[y][x])
            dataframe.append(feature_list)

    return dataframe


if __name__ == '__main__':
    # dir_path = r"C:\\Users\\today459\\Google 雲端硬碟\\[深度學習]\\share\\F100"
    dir_path = '/home/benchuang/TONG_well_inverse'
    SEQUENCE_FROM = 1
    SEQUENCE_END = 2
    pickle_name = 'p2p_{}.pkl'.format(SEQUENCE_END)

    # filename, example F0000001, F0000002
    seqs = range(SEQUENCE_FROM, SEQUENCE_END+1)
    sequence_names = [seq_name for seq_name in ['F'+str(i).zfill(7) for i in seqs]]

    # sequence_dataframe(r"C:\\Users\\today459\\Google 雲端硬碟\\[深度學習]\\share\\F100", 'F0000001')
    dataframe = [pd.DataFrame(sequence_dataframe(dir_path, sequence_name)) for sequence_name in sequence_names]
    dataframe = pd.concat(dataframe)
    print(np.shape(dataframe))

    if not os.path.exists(pickle_name):
        dataframe.to_pickle(pickle_name)