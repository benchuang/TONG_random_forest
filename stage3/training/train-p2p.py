import re
import os
import json
import sklearn
import time
import numpy as np
import pandas as pd

from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, r2_score
from sklearn.externals import joblib
from scipy.stats import spearmanr, pearsonr

import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning) 
warnings.filterwarnings("ignore", category=FutureWarning) 
warnings.filterwarnings("ignore", category=UserWarning) 


def train(pickle_data_file, model_name, ne):

    def train_and_evaluation():
        # split datasets to training and validation parts.
        if os.path.exists(model_name):
            print('load model {}...'.format(model_name))
            rf = joblib.load(model_name)
        else:
            rf = RandomForestRegressor(n_estimators=ne, oob_score=True, random_state=0, verbose=1, n_jobs=-1)
            rf.fit(X_train, y_train)

        # evaluation models
        predicted_train = rf.predict(X_train)
        predicted_test = rf.predict(X_test)
        test_score = r2_score(y_test, predicted_test)
        spearman = spearmanr(y_test, predicted_test)
        pearson = pearsonr(y_test, predicted_test)

        print('predict parameters:')
        print(rf.score)

        # print('datasets: {0}, n_estimators: {1},training spend {2} seconds'.format(_seqs-1, rf.n_estimators, elapsed))
        print('\toob_score: {0}'.format(rf.oob_score_))
        print('\tr2_score: {0}'.format(test_score))
        print('\tspearman: {0}'.format(spearman))
        print('\tpearson: {0}'.format(pearson))

    # Training start time
    start = time.time()
    # Load dataset
    dataframe = pd.read_pickle(pickle_data_file)
    feature_df = dataframe[dataframe.columns[0:82]]
    label_df = dataframe[dataframe.columns[82]]
    X_train, X_test, y_train, y_test = train_test_split(feature_df, label_df, train_size=0.8, random_state=42)
    # train and evaluate
    train_and_evaluation()

    # Training end time
    end = time.time()
    elapsed = end - start
    print('training spend {0} seconds'.format(elapsed))


if __name__ == '__main__':

    pickle_data_file = 'p2p_10.pkl'
    model_name = 'stage3_p2p_model.pkl'
    ne = 50

    train(pickle_data_file, model_name, ne)
    