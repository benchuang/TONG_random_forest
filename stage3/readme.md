# Stage3

Stage3與Stage1的不同為與四周的點位做互乘。分成兩個模式，p2p是將單對其餘三點相乘再相加。split是將四點分別互乘產生更多feature。以下為示意圖:

綠點為Stress點位值，紅點label的數值，用周圍四點的數值產生feature來做訓練。

![](img/Stage3.png)


p2p與split兩種的執行方式概念相同，需要將資料做前處理(preprocess)再做訓練(train)。



## preprocess的config解說(適用p2p,split)

![](img\preprocess-config.PNG)

* dir_path: 資料集目錄路徑。
* SEQUENCE_FROM: 前處理資料起始編號。
* SEQUENCE_END:  前處理資料結尾編號。
* pickle_name:   前處理資料的輸出檔案。


## train的config解說(適用p2p,split)

* pickle_data_file: 前處理資料的檔案位置。
* model_name: 產出的模型檔案名稱。
* ne: Randomforest樹的數量。

### p2p

train-p2p.py
![](img\train-p2p-config.PNG)

### split

train-split.py
![](img\train-split-config.PNG)



## p2p 解說和執行用法

Feature數量: 4 * 20 * 4 + 2 = 322

Stress四周點位互乘(4) * Stress Page(20) + x,y軸(2)

Feature       | value
--------------|:---------------------------:|
| feature01   | Stress01(0,0)*[Stress01(0,0)+Stress01(0,1)+Stress01(1,0)+Stress01(1,1)] |
| feature02   | Stress01(0,1)*[Stress01(0,0)+Stress01(0,1)+Stress01(1,0)+Stress01(1,1)]|
| feature03   | Stress01(1,0)*[Stress01(0,0)+Stress01(0,1)+Stress01(1,0)+Stress01(1,1)] |
| feature04   | Stress01(1,1)*[Stress01(0,0)+Stress01(0,1)+Stress01(1,0)+Stress01(1,1)] |
| ...         | ...                         |



### 執行方式

依據stage1的step1-4建置python環境。


#### step 1. 前處理

```
python preprocess-p2p.py
```

產生前處理資料集。


![](img\preprocess-file.PNG)

P.S. 與split的檔案名稱會相同，執行前先確保同名檔案清除。


#### step 2. 訓練

訓練前請注意是否有同名稱的模型檔案存在，若存在則不會重新訓練。


```
python train-p2p.py
```

訓練完成，產生模型檔案。

![](img\train_result.PNG)



## split 解說和執行用法

Feature數量: 4 * 20 + 2 = 82

Stress四周點位互乘相加(4) * Stress Page(20) + x,y軸(2)

Feature       | value
--------------|:---------------------------:|
| feature01   | Stress01(0,0)*Stress01(0,0) |
| feature02   | Stress01(0,0)*Stress01(0,1) |
| feature03   | Stress01(0,0)*Stress01(1,0) |
| feature04   | Stress01(0,0)*Stress01(1,1) |
| feature05   | Stress01(0,1)*Stress01(0,0) |
| feature06   | Stress01(0,1)*Stress01(0,1) |
| feature07   | Stress01(0,1)*Stress01(1,0) |
| feature08   | Stress01(0,1)*Stress01(1,1) |
| feature09   | Stress01(1,0)*Stress01(0,0) |
| feature10   | Stress01(1,0)*Stress01(0,1) |
| feature11   | Stress01(1,0)*Stress01(1,0) |
| feature12   | Stress01(1,0)*Stress01(1,1) |
| feature13   | Stress01(1,1)*Stress01(0,0) |
| feature14   | Stress01(1,1)*Stress01(0,1) |
| feature15   | Stress01(1,1)*Stress01(1,0) |
| feature16   | Stress01(1,1)*Stress01(1,1) |
| ...         | ...                         |


### 執行方式


依據stage1的step1-4建置python環境。

#### step 1. 前處理


```
python preprocess-split.py
```

產生前處理資料集。

![](img\preprocess-file.PNG)

P.S. 與p2p的檔案名稱會相同，執行前先確保同名檔案清除。


#### step 2. 訓練

訓練前請注意是否有同名稱的模型檔案存在，若存在則不會重新訓練。

```
python train-split.py
```

訓練完成，產生模型檔案。

![](img\train_result.PNG)


