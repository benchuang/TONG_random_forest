
# coding: utf-8

# In[1]:


# !pip install pyKriging
import numpy as np
import pyKriging
from pyKriging import kriging
import logging
import os
import json

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s : %(message)s', filename='inference.log')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(levelname)s : %(message)s')
console.setFormatter(formatter)
logging.getLogger('').addHandler(console)


# In[2]:


import os
def preprare_sample(**kwargs):
    kriging_DatasetDirectory = kwargs['kriging_DatasetDirectory']
    seq = kwargs['seq']
    page_txt = kwargs['page_txt']
    X = []
    y = []
    with open(os.path.join(kriging_DatasetDirectory, seq, page_txt), 'r') as f:
        for line in f:
            data = line.split(',')
            X.append([float(data[0])/20, float(data[1])/20])
            y.append(float(data[2].strip()))
    return np.array(X), np.array(y)

def kriging_output(**kwargs):
    kriging_ResultDirectory = kwargs['kriging_ResultDirectory']
    kriging_DatasetDirectory = kwargs['kriging_DatasetDirectory']
    seq = kwargs['seq']
    page_txt = kwargs['page_txt']
    print(seq, page_txt)
    
    X, y = preprare_sample(**kwargs)
    testfun = pyKriging.testfunctions().branin 
    k = kriging(X,y, testfunction=testfun, name='simple')
    k.train(optimizer='ga')
    row=[]
    for i in range(21):
        for j in range(21):
            coor_x = j/20
            coor_y = i/20
            pred = k.predict([coor_x, coor_y])
    #         row.append([coor_x*20, coor_y*20, pred])
            row.append(pred)
    output_data = json.dumps({"21*21":row})
        
    if not os.path.exists(os.path.join(kriging_ResultDirectory, seq)):
        os.mkdir(os.path.join(kriging_ResultDirectory, seq))
        
    with open(os.path.join(kriging_ResultDirectory, seq, page_txt.replace('txt', 'JSON')), 'w') as f:
        f.write(output_data)
    logging.info('Kriging finish {}'.format(os.path.join(kriging_ResultDirectory, seq, page_txt.replace('txt', 'JSON'))))
        


# seq = 'F0000001'
# page = 'Stress01.txt'
# kriging_output(kriging_DatasetDirectory=kriging_DatasetDirectory,
#                kriging_ResultDirectory=kriging_ResultDirectory,
#               seq=seq,
#               page=page)


# In[3]:


from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import time
class MyHandler(FileSystemEventHandler):
    def on_created(self, event):
        if event.is_directory:
            logging.info("directory created:{0}".format(event.src_path))
        else:
            kriging_DatasetDirectory = 'kriging_DatasetDirectory'
            kriging_ResultDirectory = 'kriging_ResultDirectory'
            
            path_sep_list = event.src_path.split(os.path.sep)
            page_txt = path_sep_list[-1]
            seq = path_sep_list[-2]
            if page_txt.endswith(".txt") and seq.startswith('F'):
                kriging_output(kriging_DatasetDirectory=kriging_DatasetDirectory,
                               kriging_ResultDirectory=kriging_ResultDirectory,
                               seq=seq,
                               page_txt=page_txt)
                    
if __name__ == "__main__":
    kriging_DatasetDirectory = 'kriging_DatasetDirectory'

    logging.info("Kriging Start!")
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, path=kriging_DatasetDirectory, recursive=True)
    observer.start()
    
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


# In[ ]:


# a=np.array(row).reshape([21,21])
# import matplotlib.pyplot as plt
# %matplotlib inline
# plt.imshow(a)


# In[ ]:


# ori_file='/home/cuda/benchuang/TONG_well_inverse_new/F0000001/Stress01.JSON'
# with open(ori_file, 'r') as f:
#     data = np.array(json.loads(f.read())['21*21']).reshape([21,21])  
# plt.imshow(data)

