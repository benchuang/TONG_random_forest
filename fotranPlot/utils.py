import numpy as np
import ctypes
from ctypes import *
import sys

def structCopy(src, dst):
	for field_name, field_type in src._fields_:
		setattr(dst, field_name, getattr(src, field_name))	

def getdType(srcDict):
    ndarray = np.array(srcDict)
    if ndarray.dtype == "float64":
        return np.float64
    elif ndarray.dtype == "int64":
        return np.int32

def getctypes(srcDict, struct):
    for key in srcDict:
        for field_name, field_type in struct._fields_:
            if key == field_name:
                if field_type in [c_double, c_int]:
                    value = srcDict[field_name]
                    setattr(struct, field_name, value)
                else:   
                    dtype = getdType(srcDict[field_name])
                    ndarray = np.array(srcDict[field_name], dtype=dtype)                    
                    srcDict[field_name] = ndarray #prevent pointer of ndarray reassigned
                    setattr(struct, field_name, ndarray.ctypes.data_as(field_type))                    

def getdict(struct):
    result = {}
    for memShape in struct.memShapes:
        if memShape["shape"] == None:
            value = getattr(struct, memShape["field_name"])  
        else:
            ptr = getattr(struct, memShape["field_name"])
            value = make_nd_array(ptr, memShape["shape"], dtype=memShape["dtype"])    
        result[memShape["field_name"]] = value              
    return result

def make_nd_array(c_pointer, shape, dtype=np.float64, order='C', own_data=True):
    arr_size = np.prod(shape[:]) * np.dtype(dtype).itemsize 
    if sys.version_info.major >= 3:
        buf_from_mem = ctypes.pythonapi.PyMemoryView_FromMemory
        buf_from_mem.restype = ctypes.py_object
        buf_from_mem.argtypes = (ctypes.c_void_p, ctypes.c_int, ctypes.c_int)
        buffer = buf_from_mem(c_pointer, arr_size, 0x100)
    else:
        buf_from_mem = ctypes.pythonapi.PyBuffer_FromMemory
        buf_from_mem.restype = ctypes.py_object
        buffer = buf_from_mem(c_pointer, arr_size)
    arr = np.ndarray(tuple(shape[:]), dtype, buffer, order=order)
    if own_data and not arr.flags.owndata:
        return arr.copy().tolist()
    else:
        return arr.tolist()

