from ctypes import *
import ctypes
import sys
import os
import numpy as np
import utils
from pipe import *
from os.path import join

sim = cdll.LoadLibrary(join(os.path.dirname(os.path.abspath(__file__)), 'lib','PLOT_fig.so'))

class Param( Structure ):
    pass
	
class Param2( Structure ):
    pass

def f90_plot(file_num, x=20, y=20):
        no_x = x
        no_y = y
        length = no_x*no_y
        filename = file_num

        Param._fields_ = [( "no_x", c_int ),
                          ( "no_y", c_int ),
                          ( "fig", POINTER(c_double ))]
#			  ( "filename", POINTER(c_byte))]
        Param2._fields_ = [( "filename", c_int)]

        param = Param()	
        param.memShapes = []
        param2 = Param2()	
        param2.memShapes = []

        paramC = Param()
        param2C = Param2()

        param.no_x = no_x
        param.memShapes.append({"field_name": "no_x", "dtype": np.int32, "shape": None })
        param.no_y = no_y
        param.memShapes.append({"field_name": "no_y", "dtype": np.int32, "shape": None })
        param.fig = ( c_double * length )()
        param.memShapes.append({"field_name": "fig", "dtype": np.float64, "shape": [length] })

        #param2.filename = (c_char * 256 )()
        #param2.memShapes.append({"field_name": "filename", "dtype": np.byte, "shape": [256] })
        param2.filename = filename
        param2.memShapes.append({"field_name": "filename", "dtype": np.int32, "shape": None })
		
        for i in range(400):
            param.fig[i] = 1.0*(i+1)

        #for idx, c in enumerate(filename ):
        #    param2.filename[idx] = bytes(c, encoding='utf8')

        paramDict = utils.getdict(param)
        utils.getctypes(paramDict, paramC)

        param2Dict = utils.getdict(param2)
        utils.getctypes(param2Dict, param2C)

        print(paramDict)
        print("------------------------")
        print(param2Dict)
	
#        null_fds, save = suppress()
        sim.tec_fig_2D(byref(paramC), byref(param2C))
#        resume(null_fds, save)

if __name__ == "__main__":
	f90_plot(30, x=20, y=20)	



